package nl.wur.fbit.bb;

/**
 * Utility routines for mashup jsp files
 * 
 */
public class Util {

	// name of icons directory in mashup war file and in course content directory
	public static String ICONS_DIR = "icons";
	

	/*
	* Return (sorted) array with filenames of icons (png files) that start with a number.
	*/
	public static java.io.File[] getMashupIcons(javax.servlet.http.HttpServletRequest request) {

	    java.io.File srcDir = new java.io.File(request.getServletContext().getRealPath(ICONS_DIR));
		java.io.File[] files = srcDir.listFiles(new java.io.FileFilter() {
			@Override public boolean accept(java.io.File pathname) {
				return pathname.isFile() && pathname.getName().matches("^[0-9].*\\.png$");
			};
		});
		java.util.Arrays.sort(files);
		return files;
	}

}
