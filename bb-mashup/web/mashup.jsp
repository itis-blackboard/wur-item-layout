<%@ taglib uri="/bbNG" prefix="bbNG" %>
<%@ page isErrorPage="true" %>
<%
    String course_id = request.getParameter("course_id");
    String content_id = request.getParameter("content_id");
    
    int MAX_ICONS_PER_ROW = 3;    
    String ICONS_DIR = nl.wur.fbit.bb.Util.ICONS_DIR;
    java.io.File[] icons = nl.wur.fbit.bb.Util.getMashupIcons(request);
%>
<bbNG:genericPage>
    <bbNG:form id="mashupForm" action="mashup_proc.jsp">
        <bbNG:dataCollection>
            <bbNG:step title="Content">
                <bbNG:dataElement label="Icon">
                    <table style='width: 400px; height: 100px;' align='left' border='0'><tr>
                    <% 
                        for (int i = 0; i < icons.length; ++i) {
                            String checked = i == 0 ? " checked" : "";
                            String value = "icon-"+i;
                            // add new row after every 3 icons except at the beginning
                            if (i % MAX_ICONS_PER_ROW == 0 && i > 0) {
                    %>
                        </tr><tr>
                    <%
                            }
                    %>
                        <td align='center' valign='top'>
                            <input type="radio" name="icon" value="<%= value %>"<%= checked %> >
                            <img src="<%= ICONS_DIR+"/"+icons[i].getName() %>" width="64" height="64" alt="" border="0" />
                        </td>
                    <%
                         }
                    %>
                    </tr></table>
                </bbNG:dataElement>
                <bbNG:dataElement label="Text cells">
                    <input type="radio" name="cells" value="1"> 1
                    <input type="radio" name="cells" value="2" checked> 2
                </bbNG:dataElement>
                <bbNG:dataElement label="Duration">
                    <img src="icons/duration.png" width="40" height="40" alt="" border="0" />
                    <input type="text" name="duration" size="10" value="60 min">
                    <!-- pass BB parameters to mashup_proc.jsp -->
                    <input type="hidden" name="course_id" value="<%= course_id %>">
                    <input type="hidden" name="content_id" value="<%= content_id %>">
                </bbNG:dataElement>
            </bbNG:step>
            <bbNG:stepSubmit>
                <bbNG:stepSubmitButton label="Submit"/>
            </bbNG:stepSubmit>
        </bbNG:dataCollection>
    </bbNG:form>
    <%-- code for hidding unwanted blackboard things from mashup dialog window  --%>
    <bbNG:jsBlock>
    <script language="javascript" type="text/javascript">
    document.getElementById("globalNavPageContentArea").style["position"]="absolute";
    document.getElementById("globalNavPageNavArea").style["visibility"]="hidden";
    document.getElementById("global-nav-bar").style["visibility"]="hidden";
    document.getElementById("badgeTotal").style["visibility"]="hidden";
    </script>
    </bbNG:jsBlock>
</bbNG:genericPage>
