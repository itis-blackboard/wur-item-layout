<%!
/*
* returns javascript code for the onload event of the review checkbox image
* this code will hide the review status checkbox from blackboard
*/
public String reviewImgOnLoad() {

    String result =
      "node=document.getElementById('contentListItem:@X@content.id@X@');"
            // TODO: can't get 'node &&' in while test working
    + "node=node.firstChild; while (node.className != 'details') {node=node.nextSibling};"
    + "node=node.firstChild; while (node.className != 'button-5') {node=node.nextSibling};"
    + "if (node) {"
         // hide review status button of BB
       + "node.style.display = 'none';"
         // display the correct review status icon
         // based on the text in the review status button of BB
       + "if (node.innerHTML.indexOf('Mark Review') == -1) {"
            // status is reviewed
          + "document.getElementById('not_reviewed@X@content.id@X@').style['display']='none';"
          + "document.getElementById('reviewed@X@content.id@X@').style['display']='block';"
       + "} else {"
            // status is not reviewed
          + "document.getElementById('not_reviewed@X@content.id@X@').style['display']='block';"
          + "document.getElementById('reviewed@X@content.id@X@').style['display']='none';"
       + "};"
    + " }";
    // replace single quites with escaped double-quotes
    return result.replace("'", "\\x22");
}
%>

<%

// get data from mashup form
String icon = request.getParameter("icon");
String cells = request.getParameter("cells");
String duration = request.getParameter("duration");

// basename for all icon url's in mashup theme (which must be installed seperately)
String urlBase = "/branding/themes/wurmenulayout/images/";

// get names of all action icons in mashup
java.io.File[] icons = nl.wur.fbit.bb.Util.getMashupIcons(request);

// get index number of selected action icon
int icon_index = 0;  // default if not found
java.util.regex.Matcher matcher = java.util.regex.Pattern.compile("^icon-([0-9]+)$").matcher(icon);
if (matcher.find()) {
    icon_index = Integer.parseInt(matcher.group(1));
}
java.io.File iconFile = icon_index >= 0 && icon_index < icons.length ? icons[icon_index] : icons[0];

// all mashup icon url's
String iconAction    = urlBase+iconFile.getName();
String iconDuration  = urlBase+"duration.png";
String iconCheckbox0 = urlBase+"checkbox-empty.png";
String iconCheckbox1 = urlBase+"checkbox-checked.png";

// the javascrip code for adjusting the padding of the contentListItem is placed
// in the onload method of the image because the vbte editor corrupts the closing script tag.
// "%25%0A" is a url encoded "%", the vbte editor converts "width: 100%25%0A;" to "width: 100%;"
String outputHtml =
  "<table style='width: 100%25%0A; max-width: 880px; height: 67px;' align='left' border='0'>"
+ "<tbody><tr>"
+ "<td align='left' valign='top' style='width: 90px'>"
+   "<img src='"+iconAction+"' onload='document.getElementById(\"contentListItem:@X@content.id@X@\").style[\"padding\"] = \"0px 20px\";'/>"
+ "</td>"
+ "<td align='left' valign='top' style='width: auto'>"
+   "instruction"
+ "</td>";
if ("2".equals(cells)) {
    outputHtml += 
    "<td align='right' valign='top' style='width: 150px; padding-left: 10px;'>"
  +   "link"
  + "</td>";
}
outputHtml += 
  "<td align='center' valign='top' style='width: 100px'>"
+   "<div><img src='"+iconDuration+"'/></div>"+duration
+ "</td>"
+ "<td align='left' valign='top' style='width: 30px; padding-top: 10px'>"
+   "<div>"
    // both icons take up the same place, only one of them is displayed
    // they are both needed so that BB can do it's magic with the images
+   "<img src='"+iconCheckbox0+"' id='not_reviewed@X@content.id@X@' onload='"+reviewImgOnLoad()+"' onclick='markReviewed(\"@X@content.id@X@\")'/>"
+   "<img src='"+iconCheckbox1+"' id='reviewed@X@content.id@X@' onclick='markUnreviewed(\"@X@content.id@X@\")' style='display: none'/>"
+   "</div>Done"
+ "</td>"
+ "</tr></tbody>"
+ "</table>";
            
//this prepares the embed HTML and inserts it into the VTBE/WYSIWYG editor
String WYSIWYG_WEBAPP = "/webapps/wysiwyg";

String embedUrl = blackboard.platform.plugin.PlugInUtil.getInsertToVtbePostUrl().replace(WYSIWYG_WEBAPP, "");
RequestDispatcher rd = getServletContext().getContext(WYSIWYG_WEBAPP).getRequestDispatcher(embedUrl+"?embedHtml="+outputHtml);
rd.forward(request, response);
%>
